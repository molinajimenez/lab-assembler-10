.data
.align 2

vec1:		.word 0,0,0,0,0,0,0,0,0,0
vec2:		.word 0,0,0,0,0,0,0,0,0,0
suma:		.word 0,0,0,0
N:			.word 36
formato:	.asciz "%d\n"
formatoS:	.asciz "%s\n"
mensaje:	.asciz "Imprimiendo vector..."
msjError:	.asciz "**************Ingreso mal un dato**************"
numeroVector: .word 0

.text
.align 2
.global main
.type main,%function

main:
	stmfd sp!,{lr}
	
	ldr r4,=vec1
	ldr r2,=vec2
	ldr r7,=N
	ldr r7,[r7] 	@magnitud del vector
	mov r6,#0 		@Index

	leer:
		ldr r0, = formato
		ldr r1, =numeroVector
		bl scanf

		/*Validacion de que sea numero*/
		cmp r0, #0
		beq error

		/*Guardado en Vector */
		ldr r5, =numeroVector
		ldr r5, [r5]
		str r5, [r4]

		/* condicion de salida */ 
		add r4, #4
		add r6, #4
		cmp r6,r7
		blt leer

	error:
		ldr r0, = formatoS
		ldr r1, = msjError
		bl printf

	imprimirMsj:
		ldr r0, = formatoS
		ldr r1, = mensaje
		bl printf


	imprimir:
		ldr r0, = formato
		ldr r1, [r4]
		bl printf 
		add r4, #4
		cmp r4, r7
		blt imprimir



		


	
	ldmfd sp!,{lr}
	bx lr
