.data
.align 2

vec1:		.word 0,0,0,0,0,0,0,0,0,0
vec2:		.word 0,0,0,0,0,0,0,0,0,0
suma:		.word 0,0,0,0
N:			.word 10
formato:	.asciz "%d\n"

.text
.align 2
.global main
.type main,%function

main:
	stmfd sp!,{lr}
	
	ldr r7,=vec1
	ldr r2,=vec2
	ldr r5,=N
	ldr r5,[r5] 	@Tamanio de los vectores
	mov r6,#0 		@Indice del vector

	leer:
		ldr r3,[r7],#4 	@elemento post index de vec1
		@ ingreso de datos
		@ r0 contiene formato de ingreso
		@ r1 contiene direccion donde almacena dato leido
		ldr r0,=formato
		ldr r1,[r7],#4
		bl scanf			
		str r3,[r7],#4
	
	add r6,r6,#1	@actualiza contador
	cmp r6,r5		@contador<N?
	blt leer		@si.. vaya a leer

	@Impresion del vector suma
	imprime:
		ldr r0,=formato
		ldr r1,[r7],#4
		bl printf
		add r6,r6,#1
		cmp r6,r5
		bmi imprime

	imprimeRevez:
		ldr r0, =formato
	@Salida
	mov r0,#0
	mov r3,#0
	ldmfd sp!,{lr}
	bx lr
